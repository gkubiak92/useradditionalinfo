####1. Installation

#####A. First option: Add to main composer.json file:
```
"autoload": {
    "psr-4": {
      "Greg\\UserAdditionalInfo\\": "./source/modules/greg/useradditionalinfo"
    }
  },
```
and make `composer update --no-plugins --no-scripts` and then `composer update`

####2. Required blocks in template
- template should contain block named `form_user_billing_country` in `form/fieldset/user_billing.tpl`

####3. Required template changes
Nothing
####4. Module requirements:
No additional modules are required

####5. Conflicts
No conflicts discovered yet.

####6. Known issues
No issues known yet.
####7. CRON usage
No CRON usage.
####8. Used/created Visual CMS idents. in module
- `kiraorderremindernewremind` - used for ... description
- `kiraorderremindernewfbcustomer` - used for ... description
- ...

####9. How module work?
Describe how module work.

####10. Module ERROR codes:
- `DD001` - description here. Example error code for Delivery Days (DD) which will display for user in front end and he can send this code to administrator.