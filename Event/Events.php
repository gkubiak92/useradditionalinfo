<?php
namespace Kira\SkeletonModule\Event;

use Greg\UserAdditionalInfo\UserModel;
use OxidEsales\Eshop\Core\DatabaseProvider;


class Events
{
    public static function onActivate()
    {
        $columnsToAdd = [UserModel::FIELD_ADDITIONAL_CONTACT, UserModel::FIELD_ADDITIONAL_PERSONAL];
        $sSql = "ALTER TABLE oxuser ADD $columnsToAdd[0] VARCHAR(255), $columnsToAdd[1] VARCHAR(255)";
        DatabaseProvider::getDb()->execute($sSql);
    }

    public static function onDeactivate()
    {
        $columnsToDrop = [UserModel::FIELD_ADDITIONAL_CONTACT, UserModel::FIELD_ADDITIONAL_PERSONAL];
        $sSql = "ALTER TABLE oxuser DROP $columnsToDrop[0] VARCHAR(255), $columnsToDrop[1] VARCHAR(255)";
        DatabaseProvider::getDb()->execute($sSql);
    }
}