[{$smarty.block.parent}]

<div class="form-group">
    <label class="col-lg-3">Additional contact infromation</label>
    <div class="col-lg-9">
        <input type="text" class="form-control" maxlength="128"
               name="invadr[oxuser__useradditionalinfo_contact]"
               value="[{$oxcmp_user->oxuser__useradditionalinfo_contact->value}]"
               required="false">
    </div>
</div>
<div class="form-group">
    <label class="col-lg-3">Personal preferences</label>
    <div class="col-lg-9">
        <input type="text" class="form-control" maxlength="128"
               name="invadr[oxuser__useradditionalinfo_personalpreferences]"
               value="[{$oxcmp_user->oxuser__useradditionalinfo_personalpreferences->value}]"
               required="false">
    </div>
</div>