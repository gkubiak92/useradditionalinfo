<?php


namespace Greg\UserAdditionalInfo;


class UserUpdatableFields extends UserUpdatableFields_parent
{
    protected $fieldsToAdd = [
        UserModel::FIELD_ADDITIONAL_CONTACT,
        UserModel::FIELD_ADDITIONAL_PERSONAL
    ];
    public function getUpdatableFields()
    {
        $updatableFields = parent::getUpdatableFields();
        return array_merge($updatableFields, $this->fieldsToAdd);
    }
}