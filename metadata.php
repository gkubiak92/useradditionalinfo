<?php

/* More info about all sections: https://docs.oxid-esales.com/developer/en/6.1/modules/skeleton/metadataphp/version21.html */
/**
 * Metadata version.
 */
$sMetadataVersion = '2.1';

$aModule = array(
    'id' => 'greg-useradditionalinfo',
    'title' => 'User Additional Info',
    'description' => array(
        'de' => 'Module that provides additional fields to user',
        'en' => 'Module that provides additional fields to user',
    ),
    'thumbnail' => 'module-kiratik.png',
    'version' => '1.0.0',
    'author' => 'Grzegorz Kubiak',
    'url' => '',
    'email' => 'gkubiak92@gmail.com',
    'extend' => array(),
    'controllers' => array(),
    'templates' => array(),
    'blocks' => array(),
    'extend' => array(
        \OxidEsales\Eshop\Application\Model\User\UserUpdatableFields::class => \Greg\UserAdditionalInfo\UserUpdatableFields::class
    ),
    'events' => array(
        'onActivate' => '\Greg\UserAdditionalInfo\Event\Events::onActivate',
        'onDeactivate' => '\Greg\UserAdditionalInfo\Event\Events::onDeactivate',
    ),
    'blocks' => array(
        array('template' => 'form/fieldset/user_billing.tpl', 'block'=>'form_user_billing_country', 'file'=>'/views/tpl/blocks/additional_info.tpl'),
    ),
    'settings' => array(),
);
